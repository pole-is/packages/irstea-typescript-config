# irstea-typescript-config

TSC, TSLint and Prettier configuration.

# Installation

```bash
npm add -D irstea-typescript-config
```

Also install the peer dependencies.

# Usage

In `tsconfig.json`:

```json
{
  "extends": "./node_modules/irstea-typescript-config/tsconfig.json",
  ...
}
```

In `tslint.json`:

```json
{
  "extends": [
    "irstea-typescript-config"
  ],
  ...
}
```

In `prettier.config.js`:

```js
module.exports = module.require('./node_modules/irstea-typescript-config/prettier.config');
```

